
### Getting Started

There are two methods for getting started with this repo.

#### Familiar with Git?
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone git@bitbucket.org:mohank525/reactredux.git
> cd ReduxSimpleStarter
> npm install
> npm start
```
